<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
    require("frog.php");

    $sheep = new animal("shaun");
    echo "Nama Hewan : $sheep->name <br>"; 
    echo "Jumlah kaki : $sheep->legs <br>"; 
    echo "Darah dingin ? : $sheep->coldb <br> <br>"; 

    $kodok = new Frog("buduk");
    echo "Nama Hewan : $kodok->name <br>"; 
    echo "Jumlah kaki : $kodok->legs2 <br>"; 
    echo "Darah dingin ? : $kodok->coldb <br>"; 
    echo "Suara berjalan : $kodok->jump <br><br>";
    
    require("ape.php");
    $sungokong = new ape("kera sakti");
    echo "Nama Hewan : $sungokong->name <br>"; 
    echo "Jumlah kaki : $sungokong->legs <br>"; 
    echo "Darah dingin ? : $sungokong->coldb <br>"; 
    echo "Suara Hewan : $sungokong->sound <br><br>";
    

?>

</body>
</html>